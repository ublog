#!/usr/bin/perl

#
# A minimal distributed multiuser blogging software [well ... better: hack]
# that is based on Git
# Copyright 2011 Daniel Borkmann <borkmann@gnumaniacs.org>
# Subject to the GNU GPL, version 2.
# More information: read the README
# On Debian: apt-get install txt2html
# Usage, i.e.: ublog blog.txt /var/www/htdocs/
#

use strict;
use warnings;
use HTML::TextToHTML;

my $prognam = "ublog";
my $version = "1.0";
my $line = 0;
my $verbose = 0;

use constant {
	NONE => -1,
	HEADER => 0,
	FOOTER => 1,
	ABOUT => 2,
	SETTINGS => 3,
	AUTHOR => 4,
	ENTRY => 5,
};

my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );

my $header_txt = "";
my $footer_txt = "";
my $about_txt = "";

my %authors;
my %entries;
my %settings;
my %tagsh;
my %got;

sub debug
{
	my $info = shift;
	if ($verbose) {
		print $info;
	}
}

sub parse_settings
{
	my $l = shift;
	$_ = $l;
	if (/^\s*title\s*=\s*(.*)\s*$/) {
		$settings{title} = $1;
		debug("Title: $settings{title}\n");
	} elsif (/^\s*entries_per_site\s*=\s*(\d+)\s*$/) {
		$settings{entries} = $1;
		debug("Entries/site: $settings{entries}\n");
	} elsif (/^\s*git_clone\s*=\s*(.*)\s*$/) {
		$settings{clone} = $1;
		debug("Clone URL: $settings{clone}\n");
	} elsif (/^\s*root_url\s*=\s*(.*)\s*$/) {
		$settings{root} = $1;
		debug("Root URL: $settings{root}\n");
	} elsif (/^\s*show_all_tags\s*=\s*(0|1)\s*$/) {
		$settings{tags} = $1;
		debug("Tagcloud: ".$settings{tags}."\n");
	} elsif (/^\s*show_authors\s*=\s*(0|1)\s*$/) {
		$settings{authors} = $1;
		debug("Authors: ".$settings{authors}."\n");
	} elsif (/^\s*use_txt2html\s*=\s*(0|1)\s*$/) {
		$settings{txt2html} = $1;
		debug("Txt2HTML: ".$settings{txt2html}."\n");
	} else {
		die "Syntax error in l.$line!\n";
	}
}

sub check_settings
{
	if (not $settings{title}) {
		die "Syntax error! No blog title!\n";
	}
	if (not $settings{entries}) {
		die "Syntax error! No entries/site defined!\n";
	}
	if ($settings{clone}) {
		if (not ($settings{clone} =~ /^git:\/\//)) {
			die "Syntax error! False clone URL!\n";
		}
	}
	if (not $settings{root}) {
		die "Syntax error! No blog root URL provided!\n";
	}
	if (not ($settings{root} =~ /^http:\/\//)) {
		die "Syntax error! False root URL!\n";
	}
}

sub parse_author
{
	my $author = shift;
	my $l = shift;
	$_ = $l;
	if (/^\s*name\s*=\s*(.*)\s*$/) {
		$authors{$author}->{name} = $1;
		debug("Name: ".$authors{$author}->{name}."\n");
	} elsif (/^\s*email\s*=\s*(.*)\s*$/) {
		$authors{$author}->{email} = $1;
		debug("E-Mail: ".$authors{$author}->{email}."\n");
	} elsif (/^\s*web\s*=\s*(.*)\s*$/) {
		$authors{$author}->{web} = $1;
		debug("Website: ".$authors{$author}->{web}."\n");
	} elsif (/^\s*show_nic\s*=\s*(0|1)\s*$/) {
		$authors{$author}->{usenic} = $1;
		debug("Use nic: ".$authors{$author}->{usenic}."\n");
	} else {
		die "Syntax error in l.$line!\n";
	}
}

sub check_author
{
	my $author = shift;
	if (not $authors{$author}->{name}) {
		die "Syntax error! No author name!\n";
	}
}

sub parse_entry
{
	my $time = shift;
	my $l = shift;
	$entries{$time}->{text} .= $l;
}

sub check_entry
{
	my $time = shift;
	my $nic = $entries{$time}->{author};
	my $good = 0;
	foreach (keys(%authors)) {
		if ($_ eq $nic) {
			$good = 1;
		}
	}
	if (not $good) {
		die "Syntax error! Wrong author given!\n";
	}
}

sub check_sections
{
	if (not $got{header}) {
		die "Syntax error! No header section present!\n";
	}
	if (not $got{footer}) {
		die "Syntax error! No footer section present!\n";
	}
	if (not $got{about}) {
		die "Syntax error! No about section present!\n";
	}
	if (not $got{settings}) {
		die "Syntax error! No settings section present!\n";
	}
	if (not $got{author}) {
		die "Syntax error! No author section present!\n";
	}
	if (not $got{entry}) {
		die "Syntax error! No entry section present!\n";
	}
}

sub parse
{
	my $blog = shift;
	my $state = NONE;
	my ($author, $time, @tags);
	open BLOG, "<", $blog, or die $!;
	while (<BLOG>) {
		$line++;
		next if (/^\s*#/ and $state == NONE);
		next if (/^\s+$/ and $state == NONE);
		if (/^\s*header\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = HEADER;
			$got{header} = 1;
			debug("Found header!\n");
		} elsif (/^\s*footer\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = FOOTER;
			$got{footer} = 1;
			debug("Found footer!\n");
		} elsif (/^\s*about\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = ABOUT;
			$got{about} = 1;
			debug("Found about!\n");
		} elsif (/^\s*settings\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = SETTINGS;
			$got{settings} = 1;
			debug("Found settings!\n");
		} elsif (/^\s*author\s+(\w+)\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = AUTHOR;
			$got{author} = 1;
			$author = $1;
			debug("Found author '$author'!\n");
		} elsif (/^\s*entry\s+(\d+)\s+(\w+)\s+\[([\w\s,-]*)\]\s*=\s*{\s*$/) {
			die "Syntax error in l.$line!\n" if ($state != NONE);
			$state = ENTRY;
			$got{entry} = 1;
			$time = $1;
			$author = $2;
			@tags = split(/,\s*/, $3);
			$entries{$time}->{author} = $author;
			push @{$entries{$time}->{tags}}, @tags;
			$entries{$time}->{text} = "";
			foreach my $tag (@tags) {
				$tagsh{$tag}++;
			}
			debug("Found header with '$time', '$author', '@tags'!\n");
		} elsif (/^\s*}\s*$/) {
			die "Syntax error in l.$line!\n" if ($state == NONE);
			$state = NONE;
			debug("Found close!\n");
		} elsif ($state != NONE) {
			if ($state == HEADER) {
				$header_txt .= $_;
			} elsif ($state == FOOTER) {
				$footer_txt .= $_;
			} elsif ($state == ABOUT) {
				$about_txt .= $_;
			} elsif ($state == SETTINGS) {
				parse_settings($_);
			} elsif ($state == AUTHOR) {
				parse_author($author, $_);
			} elsif ($state == ENTRY) {
				parse_entry($time, $_);
			} else {
				die "Wrong state in l.$line!\n";
			}
		} else {
			die "Syntax error in l.$line!\n";
		}
	}
	close BLOG;
}

sub check_content
{
	check_sections();
	check_settings();
	foreach (keys(%authors)) {
		check_author($_);
	}
	foreach (keys(%entries)) {
		check_entry($_);
	}
}

sub generate_index
{
	my $folder = shift;
	my $conv = new HTML::TextToHTML();
	my $sites = int(scalar(keys(%entries)) / $settings{entries}) + 1;
	my $last = scalar(keys(%entries)) % $settings{entries};
	my @keys = sort {$b <=> $a} keys(%entries);
	my @ldate = localtime(0);

	if ($last == 0) {
		if ($sites > 1) {
			$sites--;
			$last = $settings{entries};
		}
	}

	for (my $i = 0; $i < $sites; $i++) {
		my $output = "";
		my ($file, $number);

		$output .= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 ".
			   "Transitional//EN\">\n";
		$output .= "<html><head>\n";
		$output .= "<title>".$settings{title}."</title>\n";
		$output .= "<link href=\"style.css\" rel=\"stylesheet\" ".
			   "type=\"text/css\">";
		$output .= "</head><body><h2>".$settings{title}."</h2>\n";
		if ($settings{txt2html}) {
			$output .= $conv->process_chunk($header_txt);
		} else {
			$output .= $header_txt;
		}
		$output .= "<a href=\"".$settings{root}."/about.html\">About</a>, ";
		$output .= "<a href=\"".$settings{root}."/feed.xml\">RSS</a>";
		if ($settings{clone}) {
			$output .= ", Git: <a href=\"".$settings{clone}.
				   "\">".$settings{clone}."</a>";
		}
		$output .= "<br>\n<ul>";

		if ($i == $sites - 1) {
			$number = $last;
		} else {
			$number = $settings{entries};
		}
		for (my $j = 0; $j < $number; $j++) {
			my $key = shift @keys;
			my $pre = "[<a href=\"".$settings{root}.
				  "/$key.html\">p</a>";
			my $author = $entries{$key}->{author};
			my @cdate = localtime($key);

			if ($settings{authors}) {
				if ($authors{$author}->{usenic}) {
					$pre .= ", <a href=\"".$settings{root}.
						"/a_$author.xml\">$author</a>";
				} else {
					$pre .= ", <a href=\"".$settings{root}.
						"/a_$author.xml\">".
						$authors{$author}->{name}."</a>";
				}
				if ($authors{$author}->{email} or
				    $authors{$author}->{web}) {
					my $elem = 0;
					$pre .= " (";
					if ($authors{$author}->{email}) {
						$pre .= "<a href=\"mailto:".
							$authors{$author}->{email}.
							"\">e</a>";
						$elem++;
					}
					if ($authors{$author}->{web}) {
						$pre .= ", " if $elem > 0;
						$pre .= "<a href=\"".
							$authors{$author}->{web}.
							"\">w</a>";
					}
					$pre .= ")";
				}
			}
			if (scalar(@{$entries{$key}->{tags}}) > 0) {
				$pre .= ", tags: ";
				foreach my $tag (@{$entries{$key}->{tags}}) {
					$pre .= "<a href=\"".$settings{root}.
						"/t_$tag.xml\">$tag</a> ";
				}
			}
			$pre .= "] ";

			if ($ldate[3] != $cdate[3] || $ldate[4] != $cdate[4] ||
			    $ldate[5] != $cdate[5]) {
				@ldate = @cdate;
				$output .= "\n</ul>\n<h3>$abbr[$ldate[4]] ".
					   "$ldate[3], ".(1900 + $ldate[5]).
					   "</h3>\n<ul>";
			}

			if ($settings{txt2html}) {
				$output .= "\n<li>$pre".
					   $conv->process_chunk($entries{$key}->{text},
								is_fragment => 1).
					   "</li>";
			} else {
				$output .= "\n<li>$pre".$entries{$key}->{text}.
					   "</li>";
			}
		}

		if ($i == $sites - 1) {
			$output .= "\n</ul>\n<hr>\n";
		} else {
			$output .= "</ul><a href=\"".$settings{root}."/index_".
				   ($i + 1).".html\">Next</a><hr>\n";
		}
		if ($settings{tags}) {
			$output .= "All tags: ";
			foreach my $tag (keys(%tagsh)) {
				$output .= "<a href=\"".$settings{root}.
					   "/t_$tag.xml\">$tag</a> ".
					   "($tagsh{$tag}) ";
			}
		}
		if ($settings{txt2html}) {
			$output .= $conv->process_chunk($footer_txt);
		} else {
			$output .= $footer_txt;
		}
		$output .= "</body></html>";

		if ($i == 0) {
			$file = "index.html";
		} else {
			$file = "index_$i.html";
		}

		open OUT, ">", "$folder/$file" or die $!;
		print OUT $output;
		close OUT;
	}
}

sub generate_about
{
	my $folder = shift;
	my $conv = new HTML::TextToHTML();
	my $output = "";

	$output .= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 ".
		   "Transitional//EN\">\n";
	$output .= "<html><head>\n";
	$output .= "<title>".$settings{title}."</title>\n";
	$output .= "<link href=\"style.css\" rel=\"stylesheet\" ".
		   "type=\"text/css\">";
	$output .= "</head><body><h2>".$settings{title}."</h2>\n";
	if ($settings{txt2html}) {
		$output .= $conv->process_chunk($about_txt);
	} else {
		$output .= $about_txt;
	}
	$output .= "<a href=\"".$settings{root}."/index.html\">Blog</a>, ";
	$output .= "<a href=\"".$settings{root}."/feed.xml\">RSS</a>";
	if ($settings{clone}) {
		$output .= ", Git: <a href=\"".$settings{clone}."\">".
			   $settings{clone}."</a>";
	}
	$output .= "<br><hr>\n";
	if ($settings{txt2html}) {
		$output .= $conv->process_chunk($footer_txt);
	} else {
		$output .= $footer_txt;
	}
	$output .= "</body></html>";

	open OUT, ">", "$folder/about.html" or die $!;
	print OUT $output;
	close OUT;
}

sub generate_entries
{
	my $folder = shift;
	my $conv = new HTML::TextToHTML();

	foreach my $key (keys(%entries)) {
		my $output = "";
		my $author = $entries{$key}->{author};
		my @ldate = localtime($key);
		my $pre = "[<a href=\"".$settings{root}."/$key.html\">p</a>";

		if ($settings{authors}) {
			if ($authors{$author}->{usenic}) {
				$pre .= ", <a href=\"".$settings{root}.
					"/a_$author.xml\">$author</a>";
			} else {
				$pre .= ", <a href=\"".$settings{root}.
					"/a_$author.xml\">".
					$authors{$author}->{name}."</a>";
			}
			if ($authors{$author}->{email} or
			    $authors{$author}->{web}) {
				my $elem = 0;
				$pre .= " (";
				if ($authors{$author}->{email}) {
					$pre .= "<a href=\"mailto:".
						$authors{$author}->{email}.
						"\">e</a>";
					$elem++;
				}
				if ($authors{$author}->{web}) {
					$pre .= ", " if $elem > 0;
					$pre .= "<a href=\"".
						$authors{$author}->{web}.
						"\">w</a>";
				}
				$pre .= ")";
			}
		}
		if (scalar(@{$entries{$key}->{tags}}) > 0) {
			$pre .= ", tags: ";
			foreach my $tag (@{$entries{$key}->{tags}}) {
				$pre .= "<a href=\"".$settings{root}.
					"/t_$tag.xml\">$tag</a> ";
			}
		}
		$pre .= "] ";

		$output .= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 ".
			   "Transitional//EN\">\n";
		$output .= "<html><head>\n";
		$output .= "<title>".$settings{title}."</title>\n";
		$output .= "<link href=\"style.css\" rel=\"stylesheet\" ".
			   "type=\"text/css\">";
		$output .= "</head><body><h2>".$settings{title}."</h2>\n";
		if ($settings{txt2html}) {
			$output .= $conv->process_chunk($header_txt);
		} else {
			$output .= $header_txt;
		}
		$output .= "<a href=\"".$settings{root}."/about.html\">About</a>, ";
		$output .= "<a href=\"".$settings{root}."/feed.xml\">RSS</a>";
		if ($settings{clone}) {
			$output .= ", Git: <a href=\"".$settings{clone}."\">".
				   $settings{clone}."</a>";
		}
		$output .= "<br>\n";
		$output .= "<h3>$abbr[$ldate[4]] $ldate[3], ".
			   (1900 + $ldate[5])."</h3>\n<ul>";
		if ($settings{txt2html}) {
			$output .= "\n<li>$pre".
				   $conv->process_chunk($entries{$key}->{text},
							is_fragment => 1).
				   "</li>";
		} else {
			$output .= "\n<li>$pre".$entries{$key}->{text}."</li>";
		}
		$output .= "\n</ul>\n<hr>\n";
		if ($settings{txt2html}) {
			$output .= $conv->process_chunk($footer_txt);
		} else {
			$output .= $footer_txt;
		}
		$output .= "</body></html>";

		open OUT, ">", "$folder/$key.html" or die $!;
		print OUT $output;
		close OUT;
	}
}

sub textify_html
{
	my $text = shift;
	$text =~ s/&/&amp;/g;
	$text =~ s/</&lt;/g;
	$text =~ s/>/&gt;/g;
	return $text;
}

sub generate_full_rss
{
	my $folder = shift;
	my $output = "";
	my @keys = sort {$b <=> $a} keys(%entries);

	$output .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$output .= "<rss version=\"2.0\">\n";
	$output .= "<channel>\n";
	$output .= "<title>".$settings{title}."</title>\n";
	$output .= "<link>".$settings{root}."</link>\n";
	$output .= "<description>".$settings{title}.", all ".
		   "ramblings</description>\n";
	$output .= "<language>en</language>\n";

	foreach my $key (@keys) {
		$output .= "<item>\n";
		$output .= "<title>".textify_html($entries{$key}->{text}).
			   "</title>\n";
		$output .= "<link>".$settings{root}."/$key.html</link>\n";
		$output .= "<guid>".$settings{root}."/$key.html</guid>\n";
		$output .= "</item>\n";
	}

	$output .= "</channel>\n";
	$output .= "</rss>\n";

	open OUT, ">", "$folder/feed.xml" or die $!;
	print OUT $output;
	close OUT;
}

sub generate_author_rss
{
	my $folder = shift;
	my @keys = sort {$b <=> $a} keys(%entries);
	my %aitems;

	foreach my $key (@keys) {
		my $author = $entries{$key}->{author};
		if (not $aitems{$author}) {
			$aitems{$author} = "";
		}
		$aitems{$author} .= "<item>\n";
		$aitems{$author} .= "<title>".
				    textify_html($entries{$key}->{text}).
				    "</title>\n";
		$aitems{$author} .= "<link>".$settings{root}."/$key.html</link>\n";
		$aitems{$author} .= "<guid>".$settings{root}."/$key.html</guid>\n";
		$aitems{$author} .= "</item>\n";
	}

	foreach my $author (keys(%aitems)) {
		my $output = "";
		$output .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$output .= "<rss version=\"2.0\">\n";
		$output .= "<channel>\n";
		$output .= "<title>".$settings{title}."</title>\n";
		$output .= "<link>".$settings{root}."</link>\n";
		$output .= "<description>".$settings{title}.", $author\'s ".
			   "ramblings</description>\n";
		$output .= "<language>en</language>\n";
		$output .= $aitems{$author};
		$output .= "</channel>\n";
		$output .= "</rss>\n";

		open OUT, ">", "$folder/a_$author.xml" or die $!;
		print OUT $output;
		close OUT;
	}
}

sub generate_tag_rss
{
	my $folder = shift;
	my @keys = sort {$b <=> $a} keys(%entries);
	my %titems;

	foreach my $key (@keys) {
		my @tags = @{$entries{$key}->{tags}};
		foreach my $tag (@tags) {
			if (not $titems{$tag}) {
				$titems{$tag} = "";
			}
			$titems{$tag} .= "<item>\n";
			$titems{$tag} .= "<title>".
					 textify_html($entries{$key}->{text}).
					 "</title>\n";
			$titems{$tag} .= "<link>".$settings{root}.
					 "/$key.html</link>\n";
			$titems{$tag} .= "<guid>".$settings{root}.
					 "/$key.html</guid>\n";
			$titems{$tag} .= "</item>\n";
		}
	}

	foreach my $tag (keys(%titems)) {
		my $output = "";
		$output .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$output .= "<rss version=\"2.0\">\n";
		$output .= "<channel>\n";
		$output .= "<title>".$settings{title}."</title>\n";
		$output .= "<link>".$settings{root}."</link>\n";
		$output .= "<description>".$settings{title}.", $tag-tagged ".
			   "ramblings</description>\n";
		$output .= "<language>en</language>\n";
		$output .= $titems{$tag};
		$output .= "</channel>\n";
		$output .= "</rss>\n";

		open OUT, ">", "$folder/t_$tag.xml" or die $!;
		print OUT $output;
		close OUT;
	}
}

sub generate_content
{
	my $folder = shift;
	mkdir($folder, 0777);
	generate_index($folder);
	generate_about($folder);
	generate_entries($folder);
	# Yeah, sloppy and inefficient
	generate_full_rss($folder);
	generate_author_rss($folder);
	generate_tag_rss($folder);
}

sub main
{
	my $blog = shift;
	my $folder = shift;
	print "Parsing $blog blog ...\n";
	parse($blog);
	print "Checking ...\n";
	check_content();
	print "Generate html files in $folder ...\n";
	generate_content($folder);
	print "Done!\n";
}

sub help
{
	print "\n$prognam $version\n";
	print "http://gnumaniacs.org\n\n";
	print "Usage: $prognam <blog-text-file> <output-folder>\n\n";
	print "Please report bugs to <borkmann\@gnumaniacs.org>\n";
	print "Copyright (C) 2011 Daniel Borkmann <borkmann\@gnumanics.org>,\n";
	print "License: GNU GPL version 2\n";
	print "This is free software: you are free to change and redistribute it.\n";
	print "There is NO WARRANTY, to the extent permitted by law.\n\n";
	exit;
}

if ($#ARGV + 1 != 2) {
	help();
}

main(@ARGV);

